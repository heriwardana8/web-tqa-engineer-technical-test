/**
 * Web-Stockbit-test.
 * number five
 */

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;


/**
 * @author heri.wardana
 * @version $Id: FourTest.java, v 0.1 2022‐11‐26 18.59 heri.wardana
 */
public class FiveTest {

    private static final String testEndpoint = "https://api.punkapi.com/v2/beers";

    private static final String KEY_NAME = "name";

    private static final int AMOUNT_OF_DATA = 25;

    private static String fetchEndpoint(String testEndpoint) {
        StringBuilder content = new StringBuilder();
        String item;
        try {
            URL endpointUrl = new URL(testEndpoint);
            URLConnection urlConnection = endpointUrl.openConnection();
            urlConnection.addRequestProperty("User-Agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)"); // need user agent for access api

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            while ((item = bufferedReader.readLine()) != null) {
                content.append(item + "\n");
            }
            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return content.toString();
    }

    private static JSONArray contentParser(String content){
        JSONArray jsonArray = new JSONArray();
        try{
            jsonArray =  new JSONArray(content);
        } catch (Exception ignored){
        }
        return jsonArray;
    }


    @Test
    public static int testEndpoint_verifyAmountOfData(String testEndpoint){
        String endpointContent = fetchEndpoint(testEndpoint);
        JSONArray contentArray = contentParser(endpointContent);
        return contentArray.length();
    }

    public static List<String> testEndpoint_printName(String testEndpoint, String keyObject){
        List<String> listOfNames = new ArrayList<>();
        try{
            String endpointContent = fetchEndpoint(testEndpoint);
            JSONArray contentArray = contentParser(endpointContent);


            for (int i=0; i < contentArray.length(); i++) {
                JSONObject contentObject = contentArray.getJSONObject(i);
                if(contentObject.has(keyObject)){
                    listOfNames.add(contentObject.getString(keyObject));
                }
            }
        } catch (Exception ignored){

        }

        return listOfNames;
    }


    public static void main(String[] args) {

        System.out.println( "5a. Verify that the amount of data returned : \n" +
                "actual amount of data : " + testEndpoint_verifyAmountOfData(testEndpoint)
        + "\nexpected amount of data : " + AMOUNT_OF_DATA);

        System.out.println( "5b. Print all returned “name” of list that endpoint data : \n" +
                testEndpoint_printName(testEndpoint, KEY_NAME));
    }


}