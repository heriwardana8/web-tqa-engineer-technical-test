/**
 * Web-Stockbit-test.
 * number four
 */

import org.json.JSONArray;
import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;


/**
 * @author heri.wardana
 * @version $Id: FourTest.java, v 0.1 2022‐11‐26 18.59 heri.wardana
 */
public class FourTest {

    private static final String testEndpoint = "https://api.punkapi.com/v2/beers?page=2&per_page=";

    public static final int FIRST_TEST_DATA = 20;
    public static final int SECOND_TEST_DATA = 5;
    public static final int THIRD_TEST_DATA = 1;


    private static String fetchEndpoint(int numberOfData) {
        String fullEndpoint = testEndpoint + numberOfData;
        StringBuilder content = new StringBuilder();
        String item;
        try {
            URL endpointUrl = new URL(fullEndpoint);
            URLConnection urlConnection = endpointUrl.openConnection();
            urlConnection.addRequestProperty("User-Agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)"); // need user agent for access api

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            while ((item = bufferedReader.readLine()) != null) {
                content.append(item + "\n");
            }
            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return content.toString();
    }

    private JSONArray contentParser(String content){
        JSONArray jsonArray = new JSONArray();
        try{
            jsonArray =  new JSONArray(content);
        } catch (Exception ignored){
        }
        return jsonArray;
    }


    @Test
    public void testEndpoint_withFirstData(){
        String firstEndpointContent = fetchEndpoint(FIRST_TEST_DATA);
        JSONArray firstContentArray = contentParser(firstEndpointContent);

        Assert.assertEquals(firstContentArray.length(), FIRST_TEST_DATA);
    }

    @Test
    public void testEndpoint_withSecondData(){
        String secondEndpointContent = fetchEndpoint(SECOND_TEST_DATA);
        JSONArray secondContentArray = contentParser(secondEndpointContent);

        Assert.assertEquals(secondContentArray.length(), SECOND_TEST_DATA);
    }

    @Test
    public void testEndpoint_withThirdData(){
        String thirdEndpointContent = fetchEndpoint(THIRD_TEST_DATA);
        JSONArray thirdContentArray = contentParser(thirdEndpointContent);

        Assert.assertEquals(thirdContentArray.length(), THIRD_TEST_DATA);

    }



}