package StepDef;

public class Book {
    public String title;
    public String author;
    public String publisher;

    // Getter
    public String getTitle() {
        return title;
    }
    public String getAuthor() {
        return author;
    }
    public String getPublisher() {
        return publisher;
    }

    // Setter
    public void setTitle(String newTitle) {
        this.title = newTitle;
    }
    public void setAuthor(String newAuthor) {
        this.author = newAuthor;
    }
    public void setPublisher(String newPublisher) {
        this.publisher = newPublisher;
    }
}
