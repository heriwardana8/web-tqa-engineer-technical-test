package StepDef;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchBook {
    WebDriver driver;
    public final Book book = new Book();

    @Given("User go to link")
    public void user_go_to_link() throws InterruptedException {
        final String dir = System.getProperty("user.dir");
        //System.out.println("current dir = " + dir);
        System.setProperty("webdriver.chrome.driver", dir+"/driver/chromedriver");
        driver = new ChromeDriver();
        driver.get(" https://demoqa.com/books");
        Thread.sleep(1000);
    }

    @When("User in Book Store page")
    public void userInBookStorePage() {
        driver.manage().window().maximize();
    }

    @And("User search book {string}")
    public void userSearchBook(String string) {
        driver.findElement(By.id("searchBox")).sendKeys(string);
        driver.findElement(By.id("searchBox")).sendKeys(Keys.ENTER);
    }


    @Then("User see No rows found")
    public void userSeeNoRowsFound() {
        WebElement selectTitle = driver.findElement((By.xpath("/html/body/div[2]/div/div/div[2]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div/div[2]")));
        String title = selectTitle.getText();
        Assert.assertEquals(title, " ");

        WebElement selectAuthor = driver.findElement((By.xpath("/html/body/div[2]/div/div/div[2]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div/div[3]")));
        String author = selectAuthor.getText();
        Assert.assertEquals(author, " ");

        WebElement selectPublisher = driver.findElement((By.xpath("/html/body/div[2]/div/div/div[2]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div/div[4]")));
        String publisher = selectPublisher.getText();
        Assert.assertEquals(publisher, " ");

        driver.close();
        driver.quit();
    }

    @And("User search books {string}")
    public void userSearchBooks(String string) {
        driver.findElement(By.id("searchBox")).sendKeys(string);
        driver.findElement(By.id("searchBox")).sendKeys(Keys.ENTER);
    }

//    @And("User search book Git Pocket Guide")
//    public void userSearchBookGitPocketGuide() {
//        WebElement selectMyElement = driver.findElement((By.id("searchBox")));
//        selectMyElement.click();
//        selectMyElement.findElement(By.id("searchBox")).sendKeys("Git Pocket Guide");
//    }

    @And("User click book Git Pocket Guide")
    public void userClickBookGitPocketGuide() {
        WebElement selectTitle = driver.findElement((By.id("see-book-Git Pocket Guide")));
        String title = selectTitle.getText();
        book.setTitle(title);
//        System.out.println(title);

        WebElement selectAuthor = driver.findElement((By.xpath("/html/body/div[2]/div/div/div[2]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div/div[3]")));
        String author = selectAuthor.getText();
        book.setAuthor(author);
//        System.out.println(author);

        WebElement selectPublisher = driver.findElement((By.xpath("/html/body/div[2]/div/div/div[2]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div/div[4]")));
        String publisher = selectPublisher.getText();
        book.setPublisher(publisher);
//        System.out.println(publisher);

        selectTitle.click();
    }

    @Then("User see Git Pocket Guide")
    public void userSeeGitPocketGuide() {
        WebElement title = driver.findElement((By.xpath("/html/body/div[2]/div/div/div[2]/div[2]/div[2]/div[2]/div[2]/div[2]/label")));
        title.isDisplayed();

        Assert.assertEquals(title.getText(), book.getTitle());
        title.getText().equals(book.getTitle());
        System.out.println(title.getText());

        WebElement author = driver.findElement((By.xpath("/html/body/div[2]/div/div/div[2]/div[2]/div[2]/div[2]/div[4]/div[2]/label")));
        author.isDisplayed();
        Assert.assertEquals(author.getText(), book.getAuthor());
        System.out.println(book.getAuthor());

        WebElement publisher = driver.findElement((By.xpath("/html/body/div[2]/div/div/div[2]/div[2]/div[2]/div[2]/div[5]/div[2]/label")));
        publisher.isDisplayed();
        Assert.assertEquals(publisher.getText(), book.getPublisher());
        System.out.println(publisher.getText());

        driver.close();
        driver.quit();
    }



}
