package StepDef;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class SelectMenu {
    WebDriver driver;

    @Given("User Open go to link")
    public void userOpenGoToLink() throws InterruptedException {
        final String dir = System.getProperty("user.dir");
        //System.out.println("current dir = " + dir);
        System.setProperty("webdriver.chrome.driver", dir+"/driver/chromedriver");
        driver = new ChromeDriver();
        driver.get(" https://demoqa.com/select-menu");
        Thread.sleep(1000);
    }

    @When("User in select menu page")
    public void userInSelectMenuPage() {
        driver.manage().window().maximize();
    }

    @And("User choose select value Another root option")
    public void userChooseSelectValueAnotherRootOption() {
        WebElement selectMyElement = driver.findElement((By.id("withOptGroup")));
        selectMyElement.click();

        Actions firstSelect = new Actions(driver);
        firstSelect.sendKeys(Keys.chord(Keys.UP)).perform();
        firstSelect.sendKeys(Keys.ENTER).perform();

    }

    @And("User choose select one Other")
    public void userChooseSelectOneOther() {
        WebElement selectOne = driver.findElement((By.id("selectOne")));
        selectOne.click();

        Actions secondSelect = new Actions(driver);
        secondSelect.sendKeys(Keys.chord(Keys.UP)).perform();
        secondSelect.sendKeys(Keys.ENTER).perform();
    }

    @And("User choose old style select menu Aqua")
    public void userChooseOldStyleSelectMenuAqua() {
        WebElement oldSelectMenu = driver.findElement((By.id("oldSelectMenu")));
        oldSelectMenu.click();

        Actions oldSelectMenuActions = new Actions(driver);
        oldSelectMenuActions.sendKeys("Aqua");
        oldSelectMenuActions.sendKeys(Keys.ENTER).perform();
    }

    @And("User choose multi select drop down all color")
    public void userChooseMultiSelectDropDownAllColor() {
        WebElement multiSelectDropDown = driver.findElement((By.xpath("/html/body/div[2]/div/div/div[2]/div[2]/div[2]/div[7]/div/div/div/div[1]")));
        multiSelectDropDown.click();

        Actions multiSelectDropDownActions = new Actions(driver);
        multiSelectDropDownActions.sendKeys(Keys.chord(Keys.UP)).perform();
        multiSelectDropDownActions.sendKeys(Keys.ENTER).perform();
        multiSelectDropDownActions.sendKeys(Keys.ENTER).perform();
        multiSelectDropDownActions.sendKeys(Keys.ENTER).perform();
        multiSelectDropDownActions.sendKeys(Keys.ENTER).perform();
    }

    @Then("User success input all select menu")
    public void userSuccessInputAllSelectMenu() {
        driver.findElement(By.className("main-header")).isDisplayed();

        driver.close();
        driver.quit();
    }
}


